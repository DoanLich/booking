<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Party;
use Illuminate\Auth\Access\HandlesAuthorization;

class PartyPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function edit(User $user, Party $party)
    {
        return $party->owner == $user->id;
    }

    public function delete(User $user, Party $party)
    {
        return $party->owner == $user->id;
    }

    public function view(User $user, Party $party)
    {
        return true;
    }
}
