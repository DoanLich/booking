<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function index(User $loginUser)
    {
        return $loginUser->username == 'Admin';
    }

    public function edit(User $loginUser, User $user)
    {
        return $loginUser->username == 'Admin';
    }

    public function delete(User $loginUser, User $user)
    {
        return $loginUser->username == 'Admin' && $user->id != $loginUser->id;
    }

    public function view(User $loginUser, User $user)
    {
        return $loginUser->username == 'Admin';;
    }
}
