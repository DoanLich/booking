<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected function findByUsername($username)
    {
        return $this->whereUsername($username)->first();
    }

    public function parties($available = true)
    {
        if ($available == false) {
            return $this->belongsToMany(\App\Models\Party::class, 'party_members');
        }

        return $this->belongsToMany(\App\Models\Party::class, 'party_members')->whereDate('expire_date', '>=', \Carbon\Carbon::today()->toDateString());
    }

    public function displayName()
    {
        return $this->name  . ' (' . $this->username . ')';
    }
}
