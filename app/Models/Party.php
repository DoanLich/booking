<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Party extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'expire_date', 'owner', 'option_menu', 'menu_image', 'menu_list'
    ];

    public function members()
    {
        return $this->belongsToMany(\App\Models\User::class, 'party_members');
    }

    public function ownerUser()
    {
        return $this->belongsTo(\App\Models\User::class, 'owner');
    }

    public function getMenuImage()
    {
        $image = $this->menu_image;
        if ($image == null) {
            return null;
        }
        $imagePath = \Storage::disk('menu')->getAdapter()->getPathPrefix();

        return url(str_replace(DIRECTORY_SEPARATOR, '/', str_replace(public_path(), '', $imagePath)) . $image);
    }

    public function getMenuList()
    {
        $menuDatas = $this->menu_list != null ? explode(config('constant.break_menu_item'), $this->menu_list) : [];
        $menus = [];
        if (!empty($menuDatas)) {
            foreach ($menuDatas as $menu) {
                $menus[$menu] = $menu;
            }
        }

        return $menus;
    }
}
