<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartyMember extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'party_id', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class);
    }

    public function party()
    {
        return $this->belongsTo(\App\Models\Party::class);
    }

    public function detail()
    {
        return $this->hasOne(\App\Models\Order::class, 'party_member_id', 'id');
    }

    protected function findByParty($partyId, $userId)
    {
        return $this->wherePartyId($partyId)->whereUserId($userId)->first();
    }
}
