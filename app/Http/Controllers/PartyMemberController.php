<?php

namespace App\Http\Controllers;

use DB;
use Exception;
use App\Models\User;
use App\Models\Party;
use App\Models\PartyMember;
use App\Http\Requests\PartyMembers\CreatePartyMember;
use App\Http\Requests\PartyMembers\DeletePartyMember;

class PartyMemberController extends Controller
{
    public function postAdd(CreatePartyMember $request)
    {
        $data = $request->except('_token');
        $party = Party::findOrFail($request->party_id);
        $this->authorize('edit', $party);
        try {
            DB::beginTransaction();
            $members = $request->username;
            $party->members()->syncWithoutDetaching($members);
            DB::commit();
            session()->flash('success', 'The member has been added.');
            return redirect()->back();
        } catch (Exception $e) {
            $this->error($e);
            session()->flash('error', 'Failed. Please try again.');
            return redirect()->back()->withInput();
        }
    }

    public function postDelete(DeletePartyMember $request)
    {
        $data = $request->except('_token');
        $partyMember = PartyMember::findOrFail($request->id);
        $party = Party::findOrFail($partyMember->party_id);
        $this->authorize('edit', $party);
        try {
            DB::beginTransaction();
            $deleted = $partyMember->delete();
            if ($deleted != true) {
                throw new Exception("Party member can not delete", 1);
            }
            DB::commit();
            session()->flash('success', 'The member has been deleted.');
            return redirect()->back();
        } catch (Exception $e) {
            $this->error($e);
            session()->flash('error', 'Failed. Please try again.');
            return redirect()->back();
        }
    }
}
