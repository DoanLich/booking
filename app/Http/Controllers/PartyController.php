<?php

namespace App\Http\Controllers;

use DB;
use PDF;
use File;
use Storage;
use Exception;
use Carbon\Carbon;
use App\Models\Party;
use App\Models\PartyMember;
use App\Http\Requests\Parties\CreateParty;
use App\Http\Requests\Parties\UpdateParty;
use App\Http\Requests\Parties\DeleteParty;

class PartyController extends Controller
{
    public function index()
    {
        $parties = Party::where('owner', auth()->user()->id)
                    ->whereDate('expire_date', '>=', Carbon::today()->toDateString())
                    ->orderBy('expire_date', 'ASC')
                    ->paginate(config('constant.booking_food_per_page'));
        return view('parties.index', compact('parties'));
    }

    public function getAdd()
    {
        $currentDate = Carbon::today()->toDateString();
        return view('parties.add', compact('currentDate'));
    }

    public function postAdd(CreateParty $request)
    {
        $data = $request->except('_token');
        try {
            DB::beginTransaction();
            if ($data['expire_date'] == null) {
                $data['expire_date'] = Carbon::today()->toDateString();
            }
            $data['expire_date'] = Carbon::parse($data['expire_date'])->format('Y-m-d');
            $data['owner'] = auth()->user()->id;
            if ($request->option_menu != 1) {
                unset($data['menu_list']);
            } else if (!empty(trim($data['menu_list']))) {
                $data['menu_list'] = str_replace(PHP_EOL, config('constant.break_menu_item'), trim($data['menu_list']));
            }
            $party = Party::create($data);
            if ($party == null) {
                throw new Exception("Party can not save", 1);
            }
            $members = [auth()->user()->id];
            if (!empty($request->members)) {
                $members = array_merge($members, $request->members);
            }
            $party->members()->syncWithoutDetaching($members);
            $menu = $request->file('menu_file');
            if ($menu != null) {
                $extension = $menu->getClientOriginalExtension();
                $fileName = $party->id . '.' . $extension;
                $put = Storage::disk('menu')->put($fileName,  File::get($menu));

                if($put <= 0) {
                    throw new Exception('Image upload failed');
                }
                $party->update(['menu_image' => $fileName]);
            }

            DB::commit();
            session()->flash('success', 'The booking food has been added.');
            return redirect()->route('parties.list');
        } catch (Exception $e) {
            $this->error($e);
            session()->flash('error', 'Failed. Please try again.');
            return redirect()->back()->withInput();
        }
    }

    public function getEdit($id)
    {
        $party = Party::findOrFail($id);
        $this->authorize('edit', $party);
        $members = PartyMember::where('party_id', $id)->get();
        if ($party->option_menu == 1) {
            $party->menu_list = str_replace(config('constant.break_menu_item'), PHP_EOL, $party->menu_list);
        } else {
            $party->menu_list = null;
        }
        return view('parties.edit', compact('party', 'members'));
    }

    public function postEdit($id, UpdateParty $request)
    {
        $party = Party::findOrFail($id);
        $this->authorize('edit', $party);
        $data = $request->except('_token');
        try {
            DB::beginTransaction();
            if ($data['expire_date'] == null) {
                $data['expire_date'] = Carbon::today()->toDateString();
            }
            $data['expire_date'] = Carbon::parse($data['expire_date'])->format('Y-m-d');
            if ($request->option_menu != 1) {
                unset($data['menu_list']);
                $data['option_menu'] = 0;
            } else if (!empty(trim($data['menu_list']))){
                $data['menu_list'] = str_replace(PHP_EOL, config('constant.break_menu_item'), trim($data['menu_list']));
            }
            $updated = $party->update($data);
            if ($updated != true) {
                throw new Exception("Party can not save", 1);
            }
            $menu = $request->file('menu_file');
            if ($menu != null) {
                $extension = $menu->getClientOriginalExtension();
                $fileName = $party->id . '.' . $extension;
                $put = Storage::disk('menu')->put($fileName,  File::get($menu));

                if($put <= 0) {
                    throw new Exception('Image upload failed');
                }
                $party->update(['menu_image' => $fileName]);
            }
            DB::commit();
            session()->flash('success', 'The booking food has been updated.');
            return redirect()->route('parties.list');
        } catch (Exception $e) {
            $this->error($e);
            session()->flash('error', 'Failed. Please try again.');
            return redirect()->back()->withInput();
        }
    }

    public function postDelete(DeleteParty $request)
    {
        $party = Party::findOrFail($request->id);
        $this->authorize('delete', $party);
        try {
            DB::beginTransaction();
            $deleted = $party->delete();
            if ($deleted != true) {
                throw new Exception("Party can not delete", 1);
            }
            DB::commit();
            session()->flash('success', 'The booking food has been deleted.');
            return redirect()->route('parties.list');
        } catch (Exception $e) {
            $this->error($e);
            session()->flash('error', 'Failed. Please try again.');
            return redirect()->back();
        }
    }

    public function getView($id)
    {
        $party = Party::findOrFail($id);
        $this->authorize('view', $party);
        $memberDatas = PartyMember::select('party_members.*')->where('party_id', $id)
                        ->leftJoin('orders', 'orders.party_member_id', '=', 'party_members.id')
                        ->orderByDesc('orders.option1')
                        ->get();
        $members = $memberDatas->filter(function ($data) {
            return $data->detail != null;
        });
        $missMembers = $memberDatas->filter(function ($data) {
            return $data->detail == null;
        });
        $menus = $party->getMenuList();
        $loginMember = PartyMember::where('user_id', auth()->user()->id)->where('party_id', $id)->first();
        $loginMemberId = $loginMember != null ? $loginMember->id : null;
        return view('parties.view', compact('party', 'members', 'missMembers', 'menus', 'loginMemberId'));
    }

    public function exportPdf($id)
    {
        $party = Party::findOrFail($id);
        $this->authorize('view', $party);
        $members = PartyMember::select('party_members.*')->where('party_id', $id)
                    ->leftJoin('orders', 'orders.party_member_id', '=', 'party_members.id')
                    ->orderByDesc('orders.option1')
                    ->get();
        $currentDate = Carbon::today()->format('d/m/Y');
        $pdf = PDF::loadView('parties.export', compact('party', 'members', 'currentDate'));
        return $pdf->stream($party->name . '.pdf');
    }
}
