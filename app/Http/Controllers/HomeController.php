<?php

namespace App\Http\Controllers;

use DB;
use Exception;
use App\Models\PartyMember;
use Illuminate\Http\Request;
use App\Http\Requests\Profile;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parties = PartyMember::select('party_members.*')->whereUserId(auth()->user()->id)
                    ->join('parties', 'parties.id', '=', 'party_members.party_id')
                    ->whereDate('expire_date', '>=', \Carbon\Carbon::today()->toDateString())
                    ->orderBy('expire_date', 'asc')
                    ->get();

        return view('home', compact('parties'));
    }

    public function getProfile()
    {
        $user = auth()->user();
        return view('profile', compact('user'));
    }

    public function postProfile(Profile $request)
    {
        $data = $request->except('_token');
        $data = $this->_verifyRequestData($data);
        if (!empty($data['password'])) {
            $data['password'] = bcrypt($data['password']);
        }
        $user = auth()->user();
        try {
            DB::beginTransaction();
            $updated = $user->update($data);
            if ($updated != true) {
                throw new Exception("Party can not save", 1);
            }
            DB::commit();
            session()->flash('success', 'Profile has been updated.');
            return redirect()->back();
        } catch (Exception $e) {
            $this->error($e);
            session()->flash('error', 'Failed. Please try again.');
            return redirect()->back()->withInput();
        }
    }

    private function _verifyRequestData($datas)
    {
        foreach ($datas as $key => $value) {
            if ($value == null) {
                unset($datas[$key]);
            }
        }

        return $datas;
    }
}
