<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $this->authorize('index', User::class);
        $username = $request->username;
        if ($username != null) {
            $users = User::where('username', 'like', '%' . $username . '%')->paginate(config('constant.user_per_page'));
        } else {
            $users = User::paginate(config('constant.user_per_page'));
        }
        return view('users.index', compact('users', 'username'));
    }
    public function getListAjax(Request $request)
    {
        $query = $request->q;
        $users = User::where('username', 'like', '%' . $query . '%')->get();
        $responseDatas = [];
        $users->each(function ($data) use (&$responseDatas) {
            $responseDatas[] = [
                'id' => $data->id,
                'text' => $data->displayName()
            ];
        });
        return $responseDatas;
    }

    public function delete(Request $request)
    {
        if ($request->id == null) {
            session()->flash('error', 'Please select one item to delete.');
            return redirect()->back();
        }

        $user = User::findOrFail($request->id);
        $this->authorize('delete', $user);
        $user->delete();
        session()->flash('success', 'User has been deleted.');
        return redirect()->back();
    }

    public function resetPassword(Request $request)
    {
        if ($request->id == null) {
            session()->flash('error', 'Please select one item to reset password.');
            return redirect()->back();
        }

        $user = User::findOrFail($request->id);
        $this->authorize('edit', $user);
        $user->update(['password' => bcrypt(config('constant.password_default'))]);
        session()->flash('success', 'Password has been reseted to default.');
        return redirect()->back();
    }
}
