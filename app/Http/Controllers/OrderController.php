<?php

namespace App\Http\Controllers;

use DB;
use Exception;
use App\Models\User;
use App\Models\Order;
use App\Models\PartyMember;
use App\Http\Requests\Orders\CreateOrder;
use App\Http\Requests\Orders\DeleteOrder;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function getAdd($id)
    {
        $partyMember = PartyMember::findOrFail($id);
        if ($partyMember == null || $partyMember->user_id != auth()->user()->id) {
            return redirect()->home();
        }
        $menus = [];
        if ($partyMember->party->option_menu == 1 && $partyMember->party->menu_list != null) {
            $menuDatas = explode(config('constant.break_menu_item'), $partyMember->party->menu_list);
            foreach ($menuDatas as $value) {
                $menus[$value] = $value;
            }
        }
        $menus = ['' => '--- Select food ---'] + $menus;
        return view('orders.add', compact('partyMember', 'menus'));
    }

    public function postAdd(CreateOrder $request)
    {
        $data = $request->except(['_token']);
        $partyMember = PartyMember::findOrFail($request->party_member_id);
        if (empty($partyMember) || $partyMember->user_id != auth()->user()->id) {
            session()->flash('error', 'You are not allow to join this booking food.');
            return redirect()->back()->withInput();
        }
        $data['party_member_id'] = $partyMember->id;
        try {
            DB::beginTransaction();
            if (!empty($partyMember->detail)) {
                unset($data['party_member_id']);
                $partyMember->detail()->update($data);
            } else {
                $order = Order::create($data);
                if ($order == null) {
                    throw new Exception("Order can not save", 1);
                }
            }
            DB::commit();
            session()->flash('success', 'Booking successful.');
            return redirect()->home();
        } catch (Exception $e) {
            $this->error($e);
            DB::rollback();
            session()->flash('error', 'Failed. Please try again.');
            return redirect()->back()->withInput();
        }
    }

    public function postDelete(DeleteOrder $request)
    {
        $partyMember = PartyMember::findOrFail($request->id);
        if (empty($partyMember) || $partyMember->user_id != auth()->user()->id) {
            session()->flash('error', 'You are not allow to join this booking food.');
            return redirect()->back();
        }
        try {
            DB::beginTransaction();
            if (!empty($partyMember->detail)) {
                $deleted = $partyMember->detail()->delete();
                if ($deleted != true) {
                    throw new Exception("Order can not delete", 1);
                }
            }
            DB::commit();
            session()->flash('success', 'Booking has been canceled.');
            return redirect()->back();
        } catch (Exception $e) {
            $this->error($e);
            DB::rollback();
            session()->flash('error', 'Failed. Please try again.');
            return redirect()->back()->withInput();
        }
    }

    public function postOwnerAdd(CreateOrder $request)
    {
        $data = $request->except(['_token']);
        $partyMember = PartyMember::findOrFail($request->party_member_id);
        if (empty($partyMember) || $partyMember->party->owner != auth()->user()->id) {
            session()->flash('error', 'You are not allow to book for member on this booking food.');
            return redirect()->route('parties.get-view', $partyMember->party_id);
        }
        try {
            DB::beginTransaction();
            if (!empty($partyMember->detail)) {
                unset($data['party_member_id']);
                $partyMember->detail()->update($data);
            } else {
                $order = Order::create($data);
                if ($order == null) {
                    throw new Exception("Order can not save", 1);
                }
            }
            DB::commit();
            session()->flash('success', 'Booking successful.');
            return response()->json(['res' => 1, 'url' => '']);
        } catch (Exception $e) {
            $this->error($e);
            DB::rollback();
            return response()->json(['error' => 'Failed. Please try again.']);
        }
    }

    public function getOwnerEdit(Request $request)
    {
        $partyMember = PartyMember::findOrFail($request->id);
        if (empty($partyMember) || empty($partyMember->detail)) {
            return json_encode(['option1' => '', 'option2' => '', 'note' => '']);
        }

        return json_encode(['option1' => $partyMember->detail->option1, 'option2' => $partyMember->detail->option2, 'note' => $partyMember->detail->note]);
    }

    public function postOwnerDelete(DeleteOrder $request)
    {
        $partyMember = PartyMember::findOrFail($request->id);
        if (empty($partyMember) || $partyMember->party->owner != auth()->user()->id) {
            session()->flash('error', 'You are not allow to clear booking for this booking food.');
            return redirect()->back();
        }
        try {
            DB::beginTransaction();
            if (!empty($partyMember->detail)) {
                $deleted = $partyMember->detail()->delete();
                if ($deleted != true) {
                    throw new Exception("Order can not delete", 1);
                }
            }
            DB::commit();
            session()->flash('success', 'Booking has been canceled.');
            return redirect()->back();
        } catch (Exception $e) {
            $this->error($e);
            DB::rollback();
            session()->flash('error', 'Failed. Please try again.');
            return redirect()->back()->withInput();
        }
    }
}
