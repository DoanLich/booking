<?php

namespace App\Http\Requests\Parties;

use Illuminate\Foundation\Http\FormRequest;

class CreateParty extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'menu_file' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1000',
            'expire_date' => 'date|date_format:Y-m-d|after_or_equal:today',
            'option_menu' => 'in:0,1'
        ];
    }
}
