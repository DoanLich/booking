<?php

namespace App\Http\Requests\PartyMembers;

use Illuminate\Foundation\Http\FormRequest;

class CreatePartyMember extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'party_id' => 'required',
            'username' => 'required'
        ];
    }
}
