<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::statement('ALTER TABLE users AUTO_INCREMENT = 1;');
        $datas = [
            'Admin' => 'Adminitrator',
            'LichDV' => 'Doan Van Lich',
            'NganPTT1' => 'Phan Thi Thanh Ngan',
            'ThuyBTN1' => 'Bui Thi Ngoc Thuy',
            'HiepDLM' => 'Dang Le Minh Hiep',
            'DuongDQ3' => 'Dinh Quang Duong',
            'NhanHT3' => 'Ho Thanh Nhan',
            'NhoHB' => 'Huynh Ba Nho',
            'KhoaLA' => 'Le Anh Khoa',
            'HuyLH' => 'Le Hoang Huy',
            'NhiNBQ' => 'Nguyen Binh Quan Nhi',
            'VuongNDQ' => 'Nguyen Dang Quoc Vuong',
            'PhuNT4' => 'Nguyen Thanh Phu',
            'QuanNT11' => 'Nguyen Thanh Quan',
            'DoanhNT2' => 'Nguyen The Doanh',
            'BinhNT37' => 'Nguyen Trong Binh',
            'DatPT5' => 'Pham Tien Dat',
            'ChanhPM' => 'Phan Minh Chanh',
            'ThuyTT16' => 'Tran Thanh Thuy',
            'TuanTK' => 'Tu Kim Tuan',
            'BinhVN1' => 'Vu Nguyen Binh',
            'LamVT3' => 'Vu Thanh Lam',
            'HaoVTM1' => 'Vu Thi My Hao',
            'ThanhVV5' => 'Vu Van Thanh',
        ];
        $records = [];
        foreach ($datas as $username => $name) {
            $item['name'] = $name;
            $item['username'] = $username;
            $item['email'] = $username . '@fsoft.com.vn';
            $item['password'] = bcrypt('123456');
            $records[] = $item;
        }
        DB::table('users')->insert($records);
    }
}
