<?php

use Illuminate\Database\Seeder;

class PartiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('parties')->delete();
        DB::statement('ALTER TABLE parties AUTO_INCREMENT = 1;');
        DB::table('party_members')->delete();
        DB::statement('ALTER TABLE party_members AUTO_INCREMENT = 1;');
        factory(App\Models\Party::class, 1000)->create()->each(function($u) {
            $num = rand(5, 23);
            $members = [];
            for ($i = 2; $i <= $num; $i ++) {
                $members[] = $i;
            }
            $u->members()->attach($members);
        });
    }
}
