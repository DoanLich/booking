<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'username' => $faker->unique()->username,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('123456'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Party::class, function (Faker\Generator $faker) {
    $owner = rand(2, 23);
    $year = rand(2017, 2018);
    $month = rand(9, 12);
    $day = rand(11, 28);

    $date = \Carbon\Carbon::create($year,$month ,$day , 0, 0, 0);
    return [
        'name' => $faker->name,
        'description' => $faker->words(3, true),
        'expire_date' => $date,
        'owner' => $owner,
        'option_menu' => $faker->boolean,
        'menu_image' => $faker->imageUrl(),
        'menu_list' => $faker->word,
    ];
});