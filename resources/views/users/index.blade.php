@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="well main-bg">
            <div class="row">
                <div class="col-md-12">
                    <p class="h3 text-info no-margin-top main-color">Users</p>
                    {!! Form::open(['method' => 'GET', 'class' => 'form-inline']) !!}
                        <div class="form-group">
                            <label class="control-label">Username:</label>
                            {!! Form::text('username', $username, ['class' => 'form-control']) !!}
                        </div>
                        <button class="btn btn-default"><i class="fa fa-search"></i> Search</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="row">
            @foreach ($users as $key => $user)
                <div class="col-md-3 col-sm-4">
                    <div class="thumbnail main-bg">
                        <img src="img/user.png" style="max-width: 100px">
                        <div class="caption">
                            <h4>{{ $user->name }}</h4>
                            <p class="text-info main-color no-margin-bottom">
                                <i class="fa fa-user-o"></i>
                                {{ $user->username }}
                            </p>
                            <p class="text-info main-color">
                                <i class="fa fa-envelope-o"></i>
                                {{ $user->email }}
                            </p>
                            <div>
                                @can('delete', $user)
                                {!! Form::open(['route' => 'users.delete', 'style' => 'display:inline', 'class' => 'loading-form']) !!}
                                    <input type="hidden" name="id" value="{{ $user->id }}">
                                    <button class="btn btn-danger btn-sm">
                                        <i class="fa fa-trash"></i>
                                        Delete
                                    </button>
                                {!! Form::close() !!}
                                @endcan
                                @can('edit', $user)
                                {!! Form::open(['route' => 'users.reset-password', 'style' => 'display:inline', 'class' => 'loading-form']) !!}
                                    <input type="hidden" name="id" value="{{ $user->id }}">
                                    <button class="btn btn-default btn-sm">
                                        <i class="fa fa-lock"></i>
                                        Reset password
                                    </button>
                                {!! Form::close() !!}
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                {!! $users->links() !!}
            </div>
        </div>
        @if ($users->total() == 0)
            <div class="text-center">
                <img src="img/smile.png">
            </div>
            <div class="main-list text-center list-sm">
                <h4>No item found.</h4>
            </div>
        @endif
    </div>
@endsection