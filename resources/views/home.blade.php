@extends('layouts.app')

@section('content')
    <div class="container">
        @if ($parties->count() > 0)
            <div class="row">
                @php $count = 1; @endphp
                @foreach ($parties as $party)
                    <div class="col-md-6">
                        <div class="dashboard-item">
                            <div class="dashboard-item-content">
                                @include('shared.party-info', ['party' => $party->party, 'partyMember' => $party, 'no' => $count])
                            </div>
                        </div>
                    </div>
                    @php $count++; @endphp
                @endforeach
            </div>
        @else
        <div class="text-center">
            <img src="img/smile.png">
        </div>
        <div class="main-list text-center list-sm">
            <h4 class="main-color">No booking food for you.</h4>
        </div>
        @endif
    </div>
@endsection
