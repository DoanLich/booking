<span class="label label-{{ !empty($partyMember->detail) ? 'success' : 'danger' }} dashboard-item-no">{{ $no }}</span>
<p class="h3">
    {{ $party->name }}
</p>
<p class="text-info main-color">
    <i class="fa fa-user" aria-hidden="true"></i>
    Owner: {{ $party->ownerUser->displayName() }}
</p>
<div>
    <span class="text-info main-color">
        <i class="fa fa-calendar-o" aria-hidden="true"></i>
        Expire: {{ \Carbon\Carbon::parse($party->expire_date)->format('d/m/Y') }}
    </span>
    <button class="btn btn-info btn-xs pull-right" data-toggle="modal" data-target="#menu-{{ $party->id }}"><i class="fa fa-picture-o"></i> Menu</button>
</div>
<br>
<blockquote class="hint no-padding-top no-padding-bottom">
    <p>{{ $party->description }}</p>
</blockquote>
<p><span class="badge badge-info main-bg-color" style="margin-right: 10px;">1</span> @if(!empty($partyMember->detail)) {{ $partyMember->detail->option1 }} @else <span class="hint">Not set</span> @endif</p>
<p><span class="badge badge-info main-bg-color" style="margin-right: 10px;">2</span> @if(!empty($partyMember->detail)) {{ $partyMember->detail->option2 }} @else <span class="hint">Not set</span> @endif</p>
<p class="hint">Note(*): {{ !empty($partyMember->detail) ? $partyMember->detail->note : '' }}</p>
<div class="dashboard-group-button">
{!! Form::open(['route' => 'orders.post-delete', 'method' => 'POST', 'class' => 'loading-form']) !!}
    <a data-toggle="tooltip" title="Register food" href="{{ route('orders.get-add', $partyMember->id) }}" class="btn btn-success btn-xs"><i class="glyphicon glyphicon-pencil"></i></a>
    <a data-toggle="tooltip" title="View booking food detail" href="{!! route('parties.get-view', $party->id) !!}" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-eye-open"></i></a>
    {!! Form::hidden('id', $partyMember->id) !!}
    <button data-toggle="tooltip" title="Clear booking" type="submit" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-ban-circle"></i></button>
{!! Form::close() !!}
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="menu-{{ $party->id }}">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Menu</h4>
            </div>
            <div class="modal-body" style="padding: 0">
                @if ($party->option_menu == 1)
                    @php
                        $menus = $party->getMenuList();
                    @endphp
                    @if (empty($menus))
                        <div class="alert alert-info">Chưa có</div>
                    @else
                        <table class="table table-striped table-bordered" style="margin-bottom: 0">
                            <tbody>
                                @php $count = 1; @endphp
                                @foreach ($menus as $menu)
                                    <tr>
                                        <td style="width: 30px">{{ $count }}</td>
                                        <td>{{ $menu }}</td>
                                    </tr>
                                    @php $count++; @endphp
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                @else
                    <div class="text-center">
                        <img src="{{ $party->getMenuImage() }}" style="max-width: 100%">
                    </div>
                @endif
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->