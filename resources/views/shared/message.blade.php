@if(session('success'))
    <div class="alert alert-success page-message fade-in-down fade-out">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4><i class="icon fa fa-check"></i> Success !</h4>
        <span>{!! session('success') !!}</span>
    </div>
@endif
@if(session('info'))
    <div class="alert alert-info page-message fade-in-down fade-out">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4><i class="icon fa fa-info"></i> Message !</h4>
        <span>{!! session('info') !!}</span>
    </div>
@endif
@if(session('error'))
    <div class="alert alert-danger page-message fade-in-down fade-out">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4><i class="icon fa fa-ban"></i> Error !</h4>
        <span>{!! session('error') !!}</span>
    </div>
@endif
@if(session('warning'))
    <div class="alert alert-warning page-message fade-in-down fade-out">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span>{!! session('warning') !!}</span>
    </div>
@endif
@if(session('message'))
    <div class="alert alert-warning page-message fade-in-down fade-out">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4><i class="icon fa fa-warning"></i> Message !</h4>
        <span>{!! session('warning') !!}</span>
    </div>
@endif
@if(session('status'))
    <div class="alert alert-info page-message fade-in-down fade-out">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4><i class="icon fa fa-info"></i> Message !</h4>
        <span>{!! session('status') !!}</span>
    </div>
@endif
