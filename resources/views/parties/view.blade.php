@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="main-list list-lg">
            <p class="h3">
                {{ $party->name }}
            </p>
            <p class="text-info main-color">
                <i class="fa fa-user" aria-hidden="true"></i>
                Owner: {{ $party->ownerUser->displayName() }}
            </p>
            <div>
                <span class="text-info main-color">
                    <i class="fa fa-calendar-o" aria-hidden="true"></i>
                    Expire: {{ \Carbon\Carbon::parse($party->expire_date)->format('d/m/Y') }}
                </span>
                <button class="btn btn-info btn-xs pull-right" data-toggle="modal" data-target="#menu"><i class="fa fa-picture-o"></i> Menu</button>
            </div>
            <br>
            <blockquote>
                <p>{{ $party->description }}</p>
            </blockquote>
            @if ($members->count() > 0)
                <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Full name</th>
                            <th>Option 1</th>
                            <th>Option 2</th>
                            <th>Note</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $countMember = 1; @endphp
                        @foreach ($members as $member)
                            <tr>
                                <td style="width: 20px">{{ $countMember }}</td>
                                <td>{{ $member->user->displayName() }}</td>
                                <td>{{ !empty($member->detail) ? $member->detail->option1 : '' }}</td>
                                <td>{{ !empty($member->detail) ? $member->detail->option2 : '' }}</td>
                                <td>{{ !empty($member->detail) ? $member->detail->note : '' }}</td>
                                <td class="text-center" style="width: 100px">
                                    @can('edit', $party)
                                        <button data-toggle="tooltip" title="Edit" class="btn btn-default btn-xs" data-toggle="modal" data-target="#owner-add" onclick="openOwnerEdit('{{ $member->id }}', '{{ $member->user->displayName() }}')"><i class="fa fa-pencil"></i></button>
                                        {!! Form::open(['route' => 'orders.post-owner-delete', 'method' => 'POST', 'class' => 'form-inline loading-form', 'style' => 'display: inline']) !!}
                                            {!! Form::hidden('id', $member->id) !!}
                                            <button data-toggle="tooltip" title="Clear booking" type="submit" class="btn btn-warning btn-xs"><i class="glyphicon glyphicon-ban-circle"></i></button>
                                        {!! Form::close() !!}
                                        {!! Form::open(['route' => 'parties.members.post-delete', 'method' => 'POST', 'class' => 'form-inline loading-form', 'style' => 'display: inline']) !!}
                                            {!! Form::hidden('id', $member->id) !!}
                                            <button data-toggle="tooltip" title="Remove this member" type="submit" class="btn btn-danger btn-xs"><i class="fa fa-remove"></i></button>
                                        {!! Form::close() !!}
                                    @endcan
                                </td>
                            </tr>
                            @php $countMember++ @endphp
                        @endforeach
                    </tbody>
                </table>
            @endif
            @if ($missMembers->count() > 0)
                <hr>
                <blockquote>
                    <p>Member do not book food yet</p>
                </blockquote>
                <table class="table table-striped table-bordered">
                    <tbody>
                        @php $countMissMember = 1; @endphp
                        @foreach ($missMembers as $member)
                            <tr>
                                <td style="width: 20px">{{ $countMissMember }}</td>
                                <td>
                                    {{ $member->user->displayName() }}
                                    @can('edit', $party)
                                        {!! Form::open(['route' => 'parties.members.post-delete', 'method' => 'POST', 'class' => 'form-inline loading-form', 'style' => 'display: inline']) !!}
                                            {!! Form::hidden('id', $member->id) !!}
                                            <button data-toggle="tooltip" title="Remove this member" type="submit" class="btn btn-danger btn-xs pull-right"><i class="fa fa-remove"></i> Remove</button>
                                        {!! Form::close() !!}
                                        <button class="btn btn-default btn-xs pull-right" data-toggle="modal" data-target="#owner-add" onclick="openOwnerAdd('{{ $member->id }}', '{{ $member->user->displayName() }}')" style="margin-right: 5px"><i class="fa fa-plus"></i> Book</button>
                                    @endcan
                                </td>
                            </tr>
                            @php $countMissMember++ @endphp
                        @endforeach
                    </tbody>
                </table>
            @endif
            <div class="text-center">
                @can('edit', $party)
                    <a href="{!! route('parties.get-edit', $party->id) !!}" class="btn btn-success"><i class="glyphicon glyphicon-pencil"></i> Update</a>
                @endcan
                @if ($loginMemberId != null)
                    <a href="{!! route('orders.get-add', $loginMemberId) !!}" class="btn btn-info"><i class="glyphicon glyphicon-pencil"></i> Book</a>
                @endif
                <a href="{!! route('parties.list') !!}" class="btn btn-default"><i class="glyphicon glyphicon-list"></i> Back to list</a>
                <a href="{!! route('parties.export', $party->id) !!}" class="btn btn-warning" target="_blank"><i class="fa fa-file-pdf-o"></i> Export PDF</a>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="menu">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Menu</h4>
                </div>
                <div class="modal-body" style="padding: 0">
                    @if ($party->option_menu == 1)
                        @if (empty($menus))
                            <div class="alert alert-info">Not set</div>
                        @else
                            <table class="table table-striped table-bordered" style="margin-bottom: 0">
                                <tbody>
                                    @php $count = 1; @endphp
                                    @foreach ($menus as $menu)
                                        <tr>
                                            <td style="width: 30px">{{ $count }}</td>
                                            <td>{{ $menu }}</td>
                                        </tr>
                                        @php $count++; @endphp
                                    @endforeach
                                </tbody>
                            </table>
                        @endif
                    @else
                        <div class="text-center">
                            <img src="{{ $party->getMenuImage() }}" style="max-width: 100%">
                        </div>
                    @endif
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="owner-add">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title main-color">Booking for member <span id="owner-add-name"></span></h4>
                </div>
                <div class="modal-body">
                    @php $menus = ['' => '--- Select food ---'] + $menus; @endphp
                    @include('orders.owner-add')
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection
@section('js')
    $(document).ready(function() {
        $('#owner-add-form').submit(function(){
            $.ajax({
                url: this.action,
                dataType: "JSON",
                data: $(this).serialize(),
                type: "POST",
                beforeSend: function(){
                    $('.overlay').show();
                },
                success: function(data) {
                    if(data.res != null) {
                        window.location.reload();
                    } else {
                        $('#owner-add-error').empty();
                        $('#owner-add-error').append('<ul>');
                        $.each(data, function(index, error){
                            $.each(error, function(i, value){
                                $('#owner-add-error ul').append('<li>' + value + '</li>');
                            });
                        });
                        $('#owner-add-error').append('</ul>');
                        $('#owner-add-error').show();
                        $('.overlay').hide();
                    }
                },
                error: function(x){
                    $('.overlay').hide();
                    console.log(x);
                }
            });
            return false;
        });
    });
    function openOwnerAdd(party_member_id, member_name)
    {
        $('#owner-add-name').html(member_name);
        $('#owner-add-form input[name="party_member_id"]').val(party_member_id);
        $('#owner-add-form #option1').val('');
        $('#owner-add-form #option1').prop('selectedIndex',0);
        $('#owner-add-form #option2').val('');
        $('#owner-add-form #option2').prop('selectedIndex',0);
        $('#owner-add-form #note').val('');
    }
    function openOwnerEdit(party_member_id, member_name)
    {
        $.ajax({
            url: '{{ route('orders.get-owner-edit') }}',
            dataType: "JSON",
            data: {id: party_member_id},
            type: "GET",
            beforeSend: function(){
                $('.overlay').show();
            },
            success: function(data) {
                $('.overlay').hide();
                $('#owner-add-form #option1').val(data.option1);
                $('#owner-add-form #option2').val(data.option2);
                $('#owner-add-form #note').val(data.note);
            },
            error: function(x){
                $('.overlay').hide();
                console.log(x);
            }
        });
        $('#owner-add-name').html(member_name);
        $('#owner-add-form input[name="party_member_id"]').val(party_member_id);
        $('#owner-add').modal();
    }
@endsection