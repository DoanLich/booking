@extends('layouts.app')

@section('content')
<div class="container">
    <div class="main-form form-lg">
        <h3 class="title main-color">Update booking food</h3>
        @include('shared.error')
        {!! Form::open(['route' => ['parties.post-edit', $party->id], 'method' => 'POST', 'class' => 'form-horizontal loading-form', 'files' => true]) !!}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                {!! Form::label('name', 'Name', ['class' => 'col-md-4 control-label required']) !!}
                <div class="col-md-8">
                    {!! Form::text('name', $party->name, ['class' => 'form-control transparent-input', 'maxlength' => '255', 'required' => true, 'autofocus' => true]) !!}
                </div>
            </div>

            <div class="form-group{{ $errors->has('expire_date') ? ' has-error' : '' }}">
                {!! Form::label('expire_date', 'Expire date', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-8">
                    {!! Form::date('expire_date', $party->expire_date, ['class' => 'form-control transparent-input']) !!}
                </div>
            </div>

            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                {!! Form::label('description', 'Description', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-8">
                    {!! Form::textarea('description', $party->description, ['class' => 'form-control transparent-input', 'rows' => 3]) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-offset-4 col-md-8">
                    <div class="checkbox">
                        <label>
                            {!! Form::checkbox('option_menu', 1, $party->option_menu == 1) !!} Select fom existing list
                        </label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 col-md-offset-4">
                    <p class="hint">(<span class="text-required">*</span>) Only input when checkbox <span class="label label-info main-bg-color">Select fom existing list</span> is checked. Break line for one item.</p>
                </div>
            </div>
            <div class="form-group{{ $errors->has('menu_list') ? ' has-error' : '' }}">
                {!! Form::label('menu_list', 'List food', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-8">
                    {!! Form::textarea('menu_list', $party->menu_list, ['class' => 'form-control transparent-input', 'rows' => 3]) !!}
                </div>
            </div>

            <div class="form-group{{ $errors->has('menu_file') ? ' has-error' : '' }}">
                {!! Form::label('menu_file', 'Menu image', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-8">
                    {!! Form::file('menu_file', ['class' => 'form-control transparent-input']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <button type="submit" class="btn btn-success">
                        <i class="glyphicon glyphicon-ok"></i>
                        Save
                    </button>
                    <a href="{!! route('parties.list') !!}" class="btn btn-default"><i class="glyphicon glyphicon-list"></i> Back to list</a>
                    @if ($party->menu_image != null)
                    <span class="btn btn-info" data-toggle="modal" data-target="#menu"><i class="fa fa-picture-o"></i> View old menu</span>
                @endif
                </div>
            </div>
        {!! Form::close() !!}
        <h4>Add member</h4>
        {!! Form::open(['route' => 'parties.members.post-add', 'method' => 'POST', 'class' => 'loading-form']) !!}
            {!! Form::hidden('party_id', $party->id) !!}
            <div class="form-group">
                {!! Form::select('username[]', [], null, ['class' => 'form-control input-sm', 'id' => 'username', 'multiple' => true]) !!}
            </div>
            <button class="btn btn-default btn-sm" type="submit"><i class="fa fa-plus"></i> Add</button>
        {!! Form::close() !!}
        <br>
        @if (!empty($members))
            @foreach($members as $key => $member)
                {!! Form::open(['route' => 'parties.members.post-delete', 'method' => 'POST', 'class' => 'form-inline loading-form', 'style' => 'display: inline']) !!}
                    {!! Form::hidden('id', $member->id) !!}
                    <span class="badge badge-info badge-sm main-bg-color">{{ $member->user->displayName() }} <i class="fa fa-remove badge-remove-btn" onclick="$(this).parents('form').submit()"></i></span>
                {!! Form::close() !!}
            @endforeach
        @endif
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="menu">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Menu</h4>
            </div>
            <div class="modal-body text-center" style="padding: 0">
                <img src="{{ $party->getMenuImage() }}" style="max-width: 100%">
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection
@section('js')
    $(document).ready(function() {
        $("#username").select2({
            ajax: {
                url: "{{ route('users.list-ajax') }}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return { results: data };
                },
                cache: true
            }
        });
    });
@endsection
