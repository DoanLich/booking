@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="well main-bg">
            <div class="row">
                <div class="col-md-6">
                    <span class="h3 text-info main-color">My own booking food</span>
                </div>
                <div class="col-md-6">
                    <a href="{!! route('parties.get-add') !!}" class="btn btn-success pull-right"><i class="glyphicon glyphicon-plus"></i> Create</a>
                </div>
            </div>
        </div>
        <div class="row">
        @foreach ($parties as $key => $party)
            <div class="col-md-6">
                <div class="booking-food-item">
                    <div class="booking-food-item-content">
                        <span class="label label-success booking-food-item-no">{{ $key + 1 }}</span>
                        <h3> {{ $party->name }} </h3>
                        <p class="text-info main-color">
                            <i class="fa fa-calendar-o" aria-hidden="true"></i>
                            Expire: {{ \Carbon\Carbon::parse($party->expire_date)->format('d/m/Y') }}
                        </p>
                        <br>
                        <blockquote class="hint no-padding-top no-padding-bottom">
                            <p>{{ $party->description }}</p>
                        </blockquote>
                        <div class="booking-food-group-button">
                            {!! Form::open(['route' => 'parties.post-delete', 'method' => 'POST', 'class' => 'loading-form']) !!}
                                <a data-toggle="tooltip" title="View detail" href="{!! route('parties.get-view', $party->id) !!}" class="btn btn-info btn-xs"><i class="glyphicon glyphicon-eye-open"></i></a>
                                <a data-toggle="tooltip" title="Edit" href="{!! route('parties.get-edit', $party->id) !!}" class="btn btn-default btn-xs"><i class="glyphicon glyphicon-pencil"></i></a>
                                {!! Form::hidden('id', $party->id) !!}
                                <button data-toggle="tooltip" title="Delete" type="submit" class="btn btn-danger btn-xs"><i class="glyphicon glyphicon-trash"></i></button>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                {!! $parties->links() !!}
            </div>
        </div>
    </div>
@endsection