@extends('layouts.app')

@section('content')
<div class="container">
    <div class="main-form form-md">
        <h3 class="title main-color">Create booking food</h3>
        @include('shared.error')
        {!! Form::open(['route' => 'parties.post-add', 'method' => 'POST', 'class' => 'form-horizontal loading-form', 'files' => true]) !!}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                {!! Form::label('name', 'Name', ['class' => 'col-md-4 control-label required']) !!}
                <div class="col-md-8">
                    {!! Form::text('name', null, ['class' => 'form-control transparent-input', 'maxlength' => '255', 'required' => true, 'autofocus' => true]) !!}
                </div>
            </div>

            <div class="form-group{{ $errors->has('expire_date') ? ' has-error' : '' }}">
                {!! Form::label('expire_date', 'Expire date', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-8">
                    {!! Form::date('expire_date', $currentDate, ['class' => 'form-control transparent-input']) !!}
                </div>
            </div>

            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                {!! Form::label('description', 'Description', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-8">
                    {!! Form::textarea('description', null, ['class' => 'form-control transparent-input', 'rows' => 3]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-4 col-md-8">
                    <div class="checkbox">
                        <label>
                            {!! Form::checkbox('option_menu', 1) !!} Select fom existing list
                        </label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 col-md-offset-4">
                    <p class="hint">(<span class="text-required">*</span>) Only input when checkbox <span class="label label-info">Select fom existing list</span> is checked. Break line for one item.</p>
                </div>
            </div>
            <div class="form-group{{ $errors->has('menu_list') ? ' has-error' : '' }}">
                {!! Form::label('menu_list', 'List food', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-8">
                    {!! Form::textarea('menu_list', null, ['class' => 'form-control transparent-input', 'rows' => 3]) !!}
                </div>
            </div>

            <div class="form-group{{ $errors->has('menu_file') ? ' has-error' : '' }}">
                {!! Form::label('menu_file', 'Menu image', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-8">
                    {!! Form::file('menu_file', ['class' => 'form-control transparent-input']) !!}
                </div>
            </div>

            <div class="form-group{{ $errors->has('members') ? ' has-error' : '' }}">
                {!! Form::label('members', 'Members', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-8">
                    {!! Form::select('members[]', [], null, ['class' => 'form-control input-sm', 'id' => 'members', 'multiple' => true]) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <button type="submit" class="btn btn-success">
                        <i class="glyphicon glyphicon-ok"></i>
                        Save
                    </button>
                    <a href="{!! route('parties.list') !!}" class="btn btn-default"><i class="glyphicon glyphicon-list"></i> Back to list</a>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
@section('js')
    $(document).ready(function() {
        $("#members").select2({
            ajax: {
                url: "{{ route('users.list-ajax') }}",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function (data, page) {
                    return { results: data };
                },
                cache: true
            }
        });
    });
@endsection
