@extends('layouts.app')

@section('content')
<div class="container">
    <div class="main-form">
        <h3 class="title main-color">Update profile</h3>
        @include('shared.error')
        {!! Form::open(['route' => 'post-profile', 'method' => 'POST', 'class' => 'form-horizontal loading-form']) !!}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                {!! Form::label('name', 'Full name', ['class' => 'col-md-4 control-label required']) !!}
                <div class="col-md-8">
                    {!! Form::text('name', $user->name, ['class' => 'form-control transparent-input', 'maxlength' => '255', 'required' => true, 'autofocus' => true]) !!}
                </div>
            </div>

            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                {!! Form::label('username', 'Username', ['class' => 'col-md-4 control-label required']) !!}
                <div class="col-md-8">
                    {!! Form::text('username', $user->username, ['class' => 'form-control transparent-input', 'maxlength' => '255', 'required' => true]) !!}
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                {!! Form::label('email', 'Email', ['class' => 'col-md-4 control-label required']) !!}
                <div class="col-md-8">
                    {!! Form::email('email', $user->email, ['class' => 'form-control transparent-input', 'maxlength' => '255', 'required' => true]) !!}
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                {!! Form::label('password', 'Password', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-8">
                    {!! Form::password('password', ['class' => 'form-control transparent-input', 'maxlength' => '255']) !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('password_confirmation', 'Password confirmation', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-8">
                    {!! Form::password('password_confirmation', ['class' => 'form-control transparent-input', 'maxlength' => '255']) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <button type="submit" class="btn btn-success">
                        <i class="glyphicon glyphicon-ok"></i>
                        Save
                    </button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
