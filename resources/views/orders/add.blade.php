@extends('layouts.app')

@section('content')
<div class="container">
    <div class="main-form form-md">
        <h3 class="title main-color">Booking</h3>
        @include('shared.error')
        {!! Form::open(['route' => 'orders.post-add', 'method' => 'POST', 'class' => 'form-horizontal loading-form']) !!}
            <div class="form-group{{ $errors->has('party_id') ? ' has-error' : '' }}">
                {!! Form::label('party_id', 'Booking Food', ['class' => 'col-md-4 text-right']) !!}
                <div class="col-md-8">
                    {!! Form::hidden('party_member_id', $partyMember->id) !!}
                    <span>{{ $partyMember->party->name }}</span>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('owner', 'Owner', ['class' => 'col-md-4 text-right']) !!}
                <div class="col-md-8">
                    <span>{{ $partyMember->party->ownerUser->displayName() }}</span>
                </div>
            </div>

            <div class="form-group{{ $errors->has('option1') ? ' has-error' : '' }}">
                {!! Form::label('option1', 'Option 1', ['class' => 'col-md-4 control-label required']) !!}
                <div class="col-md-8">
                    @if ($partyMember->party->option_menu == 1)
                        {!! Form::select('option1', $menus, !empty($partyMember->detail) ? $partyMember->detail->option1 : null, ['class' => 'form-control transparent-input', 'required' => true]) !!}
                    @else
                        {!! Form::text('option1', !empty($partyMember->detail) ? $partyMember->detail->option1 : null, ['class' => 'form-control transparent-input', 'required' => true]) !!}
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('option2') ? ' has-error' : '' }}">
                {!! Form::label('option2', 'Option 2', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-8">
                    @if ($partyMember->party->option_menu == 1)
                        {!! Form::select('option2', $menus, !empty($partyMember->detail) ? $partyMember->detail->option2 : null, ['class' => 'form-control transparent-input']) !!}
                    @else
                        {!! Form::text('option2', !empty($partyMember->detail) ? $partyMember->detail->option2 : null, ['class' => 'form-control transparent-input']) !!}
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                {!! Form::label('note', 'Note', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-8">
                    {!! Form::textarea('note', !empty($partyMember->detail) ? $partyMember->detail->note : null, ['class' => 'form-control transparent-input', 'rows' => 3]) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <button type="submit" class="btn btn-success">
                        <i class="glyphicon glyphicon-ok"></i>
                        Save
                    </button>
                    <a href="{!! route('parties.get-view', $partyMember->party->id) !!}" target="_blank" class="btn btn-warning"><i class="glyphicon glyphicon-eye-open"></i> Booking food detail</a>
                    <span class="btn btn-info" data-toggle="modal" data-target="#menu"><i class="fa fa-picture-o"></i> Menu</span>
                    <a href="{!! route('home') !!}" class="btn btn-default"><i class="glyphicon glyphicon-home"></i> Back</a>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="menu">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Menu</h4>
            </div>
            <div class="modal-body" style="padding: 0">
                @if ($partyMember->party->option_menu == 1)
                    @if (empty($menus))
                        <div class="alert alert-info">Not set</div>
                    @else
                        <table class="table table-striped table-bordered" style="margin-bottom: 0">
                            <tbody>
                                @php array_shift($menus); @endphp
                                @php $count = 1; @endphp
                                @foreach ($menus as $menu)
                                    <tr>
                                        <td style="width: 30px">{{ $count }}</td>
                                        <td>{{ $menu }}</td>
                                    </tr>
                                    @php $count++; @endphp
                                @endforeach
                            </tbody>
                        </table>
                    @endif
                @else
                    <div class="text-center">
                        <img src="{{ $partyMember->party->getMenuImage() }}" style="max-width: 100%">
                    </div>
                @endif
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection
