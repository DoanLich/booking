<div class="alert alert-danger fade-out" id="owner-add-error" style="display: none"></div>
{!! Form::open(['route' => 'orders.post-owner-add', 'method' => 'POST', 'class' => 'form-horizontal loading-form', 'id' => 'owner-add-form']) !!}
    {!! Form::hidden('party_member_id', null) !!}
    <div class="form-group{{ $errors->has('option1') ? ' has-error' : '' }}">
        {!! Form::label('option1', 'Option 1', ['class' => 'col-md-4 control-label required']) !!}
        <div class="col-md-8">
            @if ($party->option_menu == 1)
                {!! Form::select('option1', $menus, null, ['class' => 'form-control transparent-input', 'id' => 'option1', 'required' => true]) !!}
            @else
                {!! Form::text('option1', null, ['class' => 'form-control transparent-input', 'id' => 'option1', 'required' => true]) !!}
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('option2') ? ' has-error' : '' }}">
        {!! Form::label('option2', 'Option 2', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-8">
            @if ($party->option_menu == 1)
                {!! Form::select('option2', $menus, null, ['class' => 'form-control transparent-input', 'id' => 'option2']) !!}
            @else
                {!! Form::text('option2', null, ['class' => 'form-control transparent-input', 'id' => 'option2']) !!}
            @endif
        </div>
    </div>

    <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
        {!! Form::label('note', 'Note', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-8">
            {!! Form::textarea('note', null, ['class' => 'form-control transparent-input', 'id' => 'note', 'rows' => 3]) !!}
        </div>
    </div>

    <div class="form-group">
        <div class="col-md-8 col-md-offset-4">
            <button type="submit" class="btn btn-success">
                <i class="glyphicon glyphicon-ok"></i>
                Save
            </button>
        </div>
    </div>
{!! Form::close() !!}
