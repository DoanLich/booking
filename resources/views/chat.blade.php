@extends('layouts.app')
@section('css')
    <link href="{{ asset('css/chat.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="main-form form-md">
        <h3 class="title main-color">Chat chit</h3>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <div class="message-title"></div>
                <div id="message-detail" class="message-detail">
                </div>
                <div class="form-group">
                    <textarea id="message-content" class="form-control transparent-input" rows="2" placeholder="Type a message"></textarea>
                </div>
                <button class="btn btn-info" onclick="sendMessage()"><i class="fa fa-paper-plane-o"></i> Send</button>
            </div>
            <div class="col-md-6">
                <div class="user-list">
                    @foreach ($users as $user)
                        <div class="user-item-row">
                            <div id="user-item-{{ $user->id }}" class="user-item" onclick="loadChat(this)" data-name="{{ $user->displayName() }}" data-id="{{ $user->id }}">
                                <div class="user-item-profile">
                                    <i class="fa fa-user"></i>
                                </div>
                                <p>{{ $user->displayName() }}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js-file')
    <script type="text/javascript" src="{{ asset('js/chat.js') }}"></script>
@endsection