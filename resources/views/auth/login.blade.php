@extends('layouts.app')

@section('content')
<div class="container">
    <div class="main-form">
        <h3 class="title main-color">Login</h3>
        @include('shared.error')
        {!! Form::open(['route' => 'login', 'method' => 'POST', 'class' => 'form-horizontal']) !!}
            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                {!! Form::label('username', 'Username', ['class' => 'col-md-4 control-label required']) !!}
                <div class="col-md-8">
                    {!! Form::text('username', null, ['class' => 'form-control transparent-input', 'maxlength' => '255', 'required' => true, 'autofocus' => true]) !!}
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                {!! Form::label('password', 'Password', ['class' => 'col-md-4 control-label required']) !!}
                <div class="col-md-8">
                    {!! Form::password('password', ['class' => 'form-control transparent-input', 'maxlength' => '255', 'required' => true]) !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-sign-in"></i>
                        Login
                    </button>

                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        Forgot password?
                    </a>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
