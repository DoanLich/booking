var websocket_url;
var user;
var toUser;
var logo;
var conn;
$(document).ready (function () {
    conn = new WebSocket(websocket_url);
    conn.onopen = function(e) {
        console.log("Connection established!");
    };

    conn.onmessage = function(e) {
        var data = JSON.parse(e.data);
        var item = '<div class="message-row message-left">' +
                        '<div class="message-item">' +
                            '<div class="message-profile partner">' +
                                '<i class="fa fa-user"></i>' +
                            '</div>' +
                            '<div class="message-content">' +
                                '<span>' + data.message + '</span>' +
                                '<span class="message-time">' + data.time + '</span>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
        var userData = JSON.parse(data.user);
        var loginUser = JSON.parse(user);
        if (loginUser.id == data.to_user) {
            var userId = userData.id;
            var selectedElementId = '#user-item-' + userId;
            var userItem = $(selectedElementId);
            if (!userItem.hasClass('active')) {
                loadChat(userItem);
            }
            $('.message-detail').append(item);

            // Scroll to bottom
            var detailElement = document.getElementById("message-detail");
            detailElement.scrollTop = detailElement.scrollHeight;

            notifyMessage(userData.name, data.message);
        }
    };

    var currentChatItem = $('.user-item')[0];
    loadChat(currentChatItem);
});

function sendMessage()
{
    time = getCurrentTime();
    var message = $('#message-content').val().trim();
    if (message != '') {
        var item = '<div class="message-row message-right">' +
                        '<div class="message-item">' +
                            '<div class="message-profile">' +
                                '<i class="fa fa-user"></i>' +
                            '</div>' +
                            '<div class="message-content">' +
                                '<span>' + message + '</span>' +
                                '<span class="message-time">' + time + '</span>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
        $('#message-content').val('');
        $('.message-detail').append(item);

        // Scroll to bottom
        var detailElement = document.getElementById("message-detail");
        detailElement.scrollTop = detailElement.scrollHeight;

        $('.message-detail').animate({ scrollTop:1000 }, 2000);
        conn.send(JSON.stringify({user: user, message: message, time: time, to_user: toUser}));
    }
}

function getCurrentTime()
{
    var date = new Date();
    var months = new Array();
    months[0] = "Jan";
    months[1] = "Feb";
    months[2] = "Mar";
    months[3] = "Apr";
    months[4] = "May";
    months[5] = "Jun";
    months[6] = "Jul";
    months[7] = "Aug";
    months[8] = "Sep";
    months[9] = "Oct";
    months[10] = "Nov";
    months[11] = "Dec";
    var month = months[date.getMonth()];
    var day = date.getDay();
    var year = date.getFullYear();
    var hour = date.getHours();
    var minute = date.getMinutes();

    return hour + ':' + minute + ' ' + month + ' ' + day + ', ' + year;
}

function loadChat(element)
{
    var name = $(element).data('name');
    var id = $(element).data('id');
    $('.message-title').html(name);
    $('.user-item').removeClass('active');
    $(element).addClass('active');
    toUser = id;
    reloadChatDetail();
}

function reloadChatDetail()
{
    $('.message-detail').empty();
}

function notifyMessage(fromUser, message) {
    if (!window.Notification) {
        alert("Sorry, Notification Not supported in this Browser!");
    } else {
        if (Notification.permission === 'default') {
            Notification.requestPermission(function(p) {
                if (p === 'denied')
                    alert('You have denied Notification from this site');
                else {
                    notify = new Notification(fromUser, {
                        body: message,
                        icon: logo
                    });
                }
            });
        } else {
            notify = new Notification(fromUser, {
                body: message,
                icon: logo  
                // You Can give image Link to change notification Icon.
            });
        }
    }
}