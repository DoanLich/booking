<?php 

return [
    'booking_food_per_page' => 2,
    'user_per_page' => 8,
    'break_menu_item' => '|',
    'password_default' => '123456',
    'websocket_url' => env('WEBSOCKET_URL', 'ws://10.88.136.22:8090'),
];