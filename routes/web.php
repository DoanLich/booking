<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    if (auth()->check()) {
        return redirect('/home');
    }
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group([
    'middleware' => 'auth'
], function () {
    Route::get('/parties', 'PartyController@index')->name('parties.list');
    Route::get('/parties/add', 'PartyController@getAdd')->name('parties.get-add');
    Route::post('/parties/add', 'PartyController@postAdd')->name('parties.post-add');
    Route::get('/parties/edit/{id}', 'PartyController@getEdit')->name('parties.get-edit');
    Route::post('/parties/edit/{id}', 'PartyController@postEdit')->name('parties.post-edit');
    Route::post('/parties/delete', 'PartyController@postDelete')->name('parties.post-delete');
    Route::get('/parties/view/{id}', 'PartyController@getView')->name('parties.get-view');
    Route::get('/parties/export/{id}', 'PartyController@exportPdf')->name('parties.export');

    Route::post('/parties/members/add', 'PartyMemberController@postAdd')->name('parties.members.post-add');
    Route::post('/parties/members/delete', 'PartyMemberController@postDelete')->name('parties.members.post-delete');

    Route::get('/booking/{id}', 'OrderController@getAdd')->name('orders.get-add');
    Route::post('/booking', 'OrderController@postAdd')->name('orders.post-add');
    Route::post('/owner-booking', 'OrderController@postOwnerAdd')->name('orders.post-owner-add');
    Route::get('/owner-edit', 'OrderController@getOwnerEdit')->name('orders.get-owner-edit');
    Route::post('/owner-delete', 'OrderController@postOwnerDelete')->name('orders.post-owner-delete');
    Route::post('/bookings/delete', 'OrderController@postDelete')->name('orders.post-delete');
    Route::get('/profile', 'HomeController@getProfile')->name('get-profile');
    Route::post('/profile', 'HomeController@postProfile')->name('post-profile');

    Route::get('/users', 'UserController@index')->name('users.list');
    Route::get('/users/list-ajax', 'UserController@getListAjax')->name('users.list-ajax');
    Route::post('/users/delete', 'UserController@delete')->name('users.delete');
    Route::post('/users/reset-password', 'UserController@resetPassword')->name('users.reset-password');

    Route::get('/chat', function () {
        $users = \App\Models\User::where('id', '<>', auth()->user()->id)->orderBy('username')->get();
        return view('chat', compact('users'));
    });
});
